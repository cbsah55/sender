First choose appropriate JDK module in you IDE:
This can be accomplished by:
Go to :
File > project structure > project > 
under project SDK choose your JDK version.


If you are not able to Run the application(Play button greyed out)

Go through the directory 'src' located at root folder of your project.
There you will find two java classes.
Open Any of these two, There you will find a small play button beside the 
public static void main(String[] args)  method.

Click it and choose Run or use shortcut `ctrl + shift+ f10`(for windows) `& ⌃⇧R` (for mac).

If you encounter No file exception create a file (AnyFileName.comp) inside outbox directory 
located in root directory of the project.

*Note : File name coud be anything(add some data to file to see if encryption is working).

#######################################################################################################

1. Install maven into the system
(in ubbuntu based use command)
`sudo apt install maven`
`brew install maven`(For MacOS)
May vary depending on your system

2. Open the terminal inside lib directory

3. Run the following commands through your terminal

-> `mvn install:install-file -Dfile=pgplib-3.2.2.jar -DgroupId=com.cbs.pgplib -DartifactId=pgplib -Dversion=3.2.2 -Dpackaging=jar`

-> `mvn install:install-file -Dfile=bcpg-lw-jdk15on-1.68.jar -DgroupId=com.cbs.bcpg-lw-jdk15on -DartifactId=bcpg-lw-jdk15on -Dversion=1.68 -Dpackaging=jar`

-> `mvn install:install-file -Dfile=bcprov-lw-jdk15on-1.68.jar -DgroupId=com.cbs.bcprov-lw-jdk15on -DartifactId=bcprov-lw-jdk15on -Dversion=1.68 -Dpackaging=jar`

##############################################################################################

Compilation and generation of jar:
1. Open the project
2. Run maven clean 
3. Run maven packag


[2 and 3 needs to be performed everytime you change the source code, You will get new jar file each time.]

A jar file named Sender.jar can be found inside target directory of your root project
Copy the jar file to the project directory i.e. root directory of your project.
Note * - Jar file and the directories outbox,keys,enctypted,trash should be within the same scope.

Open up your terminal from project directory and execute 
`java -jar Sender.jar`

This will move all the files from Encrypted directory to trash and if email function is enabled all the files will be emailed to desired email list and the files will be moved to trash directory.




