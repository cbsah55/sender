import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class Sender {
    //returns the file extension
    static String getFileExtension(File file) {
        if (file.getName().contains("."))
            return file.getName().substring(file.getName().lastIndexOf("."));
        else
            return "No file with .comp extenxion";

    }
    public static boolean sendMail(String from, String password, List<String> to, File file){
        AtomicBoolean mailSuccess = new AtomicBoolean(false);
        //Get properties object
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        //get Session
        Session session = Session.getDefaultInstance(props, new Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(from,password);
            }
        });
        try {
            MimeMessage message = new MimeMessage(session);
            to.forEach(toAddress->{
                try {
                    message.setFrom(new InternetAddress(from));
                    message.addRecipient(Message.RecipientType.TO,new InternetAddress(toAddress));
                    message.setSubject("Encrypted file");

                    BodyPart messageBodyPart = new MimeBodyPart();
                    messageBodyPart.setText("This is message body");

                    Multipart multipart = new MimeMultipart();
                    multipart.addBodyPart(messageBodyPart);

                    MimeBodyPart attachmentPart = new MimeBodyPart();
                    attachmentPart.attachFile(new File(file.getAbsolutePath()));
                    multipart.addBodyPart(attachmentPart);
                    message.setContent(multipart);

                    Transport.send(message);
                    mailSuccess.set(true);

                    System.out.println("Done");

                } catch (MessagingException | IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mailSuccess.get();
    }
    public static void main(String[] args) throws IOException {


        File encryptedDir = new File("encrypted/");
        File trashDir = new File("trash/");
        //Mail list
        List<String> mailList = new ArrayList<>();
        mailList.add("example@gmail.com");

        File file1 = new File("./lockfile.lock");
        if (file1.exists()) {
            throw new IOException("instance is running");
        } else {
            file1.createNewFile();
        }
        //Mailing all the files

        File[] listOfEncryptedFiles = encryptedDir.listFiles();
        assert listOfEncryptedFiles != null;
        if (!(listOfEncryptedFiles.length<=0)){
            for (File file : listOfEncryptedFiles){
                try (RandomAccessFile fileA = new RandomAccessFile(file.getAbsolutePath(), "rw");
                     FileChannel channel = fileA.getChannel();
                     FileLock lock = channel.lock()) {
                    if (getFileExtension(file).equals(".comp")){
                        //uncomment it and provide valid gmail username and password
                  /*if (sendMail("enterYourMail","enterYourAppPassword",mailList,file)){
                    removeFromEncryptedToTrash(encryptedDir, trashDir, file);
                    }*/
                        //if above is used remove this
                        removeFromEncryptedToTrash(encryptedDir, trashDir, file);
                        lock.release();
                    }
                } catch (OverlappingFileLockException | IOException exception) {
                    exception.printStackTrace();
                }

            }

        }else {
            deleteLock();
        }
        //Loops through trash folder to delete files without .comp and create days older than 30 days
        deleteFilesByCreatedDate(trashDir.listFiles());

        deleteLock();

    }

    private static void deleteLock() {
        File lockfile = new File( "./lockfile.lock");
        if (lockfile.exists()){
        lockfile.deleteOnExit();
        }
    }

    public static void deleteFilesByCreatedDate(File[] listOfFiles) {
        assert listOfFiles != null;
        if (!(listOfFiles.length <= 0)) {
            System.out.println("Checking if file is created thirty days earlier");
            Arrays.stream(listOfFiles).sequential().forEach(file -> {
                try {
                    BasicFileAttributes attributes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
                    String extension = getFileExtension(file);
                    FileTime createdDate = attributes.creationTime();
                    if (!extension.equals(".comp") && createdDate.toMillis() > TimeUnit.DAYS.toMillis(30)) {
                        file.delete();
                        System.out.println("File Deletion success.....");
                    }else {
                        System.out.println("No files is created older than 30 days....");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public static void removeFromEncryptedToTrash(File encryptedDir, File trashDir, File file) throws IOException {
        Files.copy(file.toPath(), (new File(trashDir.getAbsolutePath() + "/" + file.getName())).toPath(), StandardCopyOption.REPLACE_EXISTING);
        System.out.println(file.getName() + " Successfully added to trash");
        File fileToDelete = new File(encryptedDir.getAbsolutePath() + "/" + file.getName());
        fileToDelete.delete();
        System.out.println(fileToDelete.getName() + " successfully deleted from " + encryptedDir.getName());
    }
}
